/*
   TO DO
   V -Process relative X,Y position. (with the angle and distance)
   V-Directions functions
   V -Interrupt distances
   X -Handle distance traveled with the sonar
   
    note :
    each distance interrupt update the coordinates

*/
struct coordinates {
  double x;//position X
  double y;//position Y
  double a;//Angle orientation
  //double d;//Distance to goal or distance traveled
};

double AngleMapOffset = 0; // determine the offset relative to the imu and the map (imu + AngleMapOffset) = map 
coordinates vac = {0, 0, 0};
coordinates objective = {0, 0, 0};

bool getDist = false;//determines if we have to mesure distances (for example if we turn we don't)
bool stopped = true;

void setup()
{
  Serial.begin(115200);
  stop();
  Serial.println("Start");
  initServer();
  initMovement();
  initMPU();
  radarsInit();
  setObjectives(0, 0);
  Serial.println("loop");
}

void loop()
{
  //Handle client requests (every 100ms) slow
  clientServer();

  //trigger the radar
  //trigger();

  //Update Yaw
  getYaw();

  //handle movements every 10ms
  handleMovement();

  //handle the transmission if a bump occures
  handleBump();

  delay(50);
}
