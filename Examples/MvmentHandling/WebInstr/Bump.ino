const byte BumpPinL = 4; //not defined yet
const byte BumpPinR = 5; //not defined yet


volatile int bumpCounterLeft = 0;//used in the interrupt

volatile int bumpCounterRight = 0;//used in the interrupt

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;


void IRAM_ATTR handleBumbL() {
  portENTER_CRITICAL_ISR(&mux);
  stop();
  bumpCounterLeft = 1;
  portEXIT_CRITICAL_ISR(&mux);
}


void IRAM_ATTR handleBumbR() {
  portENTER_CRITICAL_ISR(&mux);
  stop();
  bumpCounterRight = 1;
  portEXIT_CRITICAL_ISR(&mux);
}



void initBumber() {
  pinMode(BumpPinL, INPUT_PULLUP);//left init
  attachInterrupt(digitalPinToInterrupt(BumpPinL), handleBumbL, FALLING);

  pinMode(BumpPinR, INPUT_PULLUP);//right init
  attachInterrupt(digitalPinToInterrupt(BumpPinR), handleBumbL, FALLING);
}

void handleBump() {


  if (bumpCounterLeft > 0) {
    portENTER_CRITICAL(&mux);
    bumpCounterLeft = 0;
    portEXIT_CRITICAL(&mux);

    //Send to rasp the BUMP !!!
  }
  if (bumpCounterRight > 0) {
    portENTER_CRITICAL(&mux);
    bumpCounterRight = 0;
    portEXIT_CRITICAL(&mux);

    //Send to rasp the BUMP !!!
  }
}
