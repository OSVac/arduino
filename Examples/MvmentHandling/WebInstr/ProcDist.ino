

#define trigPin 33
#define echoPinF 34
#define echoPinR 35


#define TIMER_US 50                                   // 50 uS timer duration 
#define TICK_COUNTS 600                              // 50 mS worth of timer ticks

volatile long echo_startF = 0;                         // Records start of echo pulse
volatile long echo_endF = 0;                           // Records end of echo pulse
volatile long echo_durationF = 0;                      // Duration - difference between end and start

volatile long echo_startR = 0;                         // Records start of echo pulse
volatile long echo_endR = 0;                           // Records end of echo pulse
volatile long echo_durationR = 0;                      // Duration - difference between end and start


volatile int trigger_time_count = 10;                  // Count down counter to trigger pulse time
volatile long range_flasher_counter = 0;              // Count down counter for flashing distance LED


hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;


#define MaxDist 3
#define MaxRelativeDelta 0,1 // in %

double oldRear;
double oldFront;

/* On met une distance que l'on ne peux pas passer entre deux mesures (dist parcourue max)
    Si la mesure est trop grande (on la discard)

   Si les deux valeurs sont "OK"
   Mesurer l'ecart entre les delta,
      Si ecart > 10% de leur moyenne, on prend le capteur le plus proche de l'obstacle (probablement plus juste)

   Sinon on fait la moyenne des deux

   On envoie la distance à la fonction de positionnement

  On peut lancer delta quand on envoie la fonction move (on prend les deux dernieres valeurs)

  le son parcours 34cm/ms donc on peut lancer un ping tout les 20ms (ca parrait raisonnable)
*/


// --------------------------
// timerIsr() 50uS second interrupt ISR()
// Called every time the hardware timer 1 times out.
// --------------------------
void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&mux);

  static volatile int state = 0;                 // State machine variable

  if (!(--trigger_time_count))                   // Count to 200mS
  { // Time out - Initiate trigger pulse
    trigger_time_count = TICK_COUNTS;           // Reload
    state = 1;                                  // Changing to state 1 initiates a pulse
  }
  switch (state)                                 // State machine handles delivery of trigger pulse
  {
    case 0:                                      // Normal state does nothing
      break;

    case 1:                                      // Initiate pulse
      digitalWrite(trigPin, HIGH);              // Set the trigger output high
      state = 2;                                // and set state to 2
      break;

    case 2:                                      // Complete the pulse
    default:
      digitalWrite(trigPin, LOW);               // Set the trigger output low
      state = 0;                                // and return state to normal 0
      //syncRadars2();
      break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}


#define MEAN_COUNT 10 // the number of distance readings that we keep 
#define MAX_DELTA 5 //
#define MAX_DIST 350 //
#define MIN_DIST 10

double old_r = 0;//get the distances
double old_f = 0;
double r = 0;
double f = 0;
double meanV = 0;

void syncRadars() { //this function define the real traveled distance
  old_f = f;
  old_r = r;
  r = echo_durationR / 58.0;//get the distances
  f = echo_durationF / 58.0;
  
  //Serial.println("New recording: " + String(f));
  double delta_f = old_f - f;//get the distance travelled since last reading
  double delta_r = r - old_r;
  //Serial.println("New delta: " + String(delta_f));
  if(!stopped)Serial.println("New deltaf: " + String(delta_f));
  
  if(!stopped)Serial.println("New deltar: " + String(delta_r));

  if (old_f < MAX_DIST && old_f > MIN_DIST && f < MAX_DIST && f > MIN_DIST && abs(delta_f) < MAX_DELTA) addToList(delta_f);//if the value is in the ranges we add it to the "mean" stack
  if (old_r < MAX_DIST && old_r > MIN_DIST && r < MAX_DIST && r > MIN_DIST && abs(delta_r) < MAX_DELTA) addToList(delta_r);
  if(getDist)updateCoord(getMeanDists());

  /*
  double mean[] = {0, 0};
  if (old_f < MAX_DIST && old_f > MIN_DIST && f < MAX_DIST && f > MIN_DIST && abs(delta_f) < MAX_DELTA) mean[0] = delta_f;//if the value is in the ranges we add it to the "mean" stack
  if (old_r < MAX_DIST && old_r > MIN_DIST && r < MAX_DIST && r > MIN_DIST && abs(delta_r) < MAX_DELTA) mean[1] = delta_r;
  if (mean[0] != 0 && mean[1] != 0) meanV = (mean[0] + mean[1]) / 2;
  else if (mean[0] == 0 && mean[1] != 0) meanV = mean[1];
  else if (mean[0] != 0 && mean[1] == 0) meanV = mean[0];
  //else mean = old mean.
  if (getDist)updateCoord(meanV);
  */
  
}


byte indx = 0;
double dists[MEAN_COUNT];

//add the reading to the list
void addToList(double newDist) {
  indx = (indx + 1) % MEAN_COUNT;
  dists[indx] = newDist;
}


void reset_dists() {
  for (int i = 0; i < MEAN_COUNT; i++) {
    dists[i] = NULL;
  }
}


//process the readings and make a mean of the *correct* readings
double getMeanDists() {
  double sum = 0;
  int ZeroComp = 0;// this value take track of 'zero' values and don't take it into account for the final count.
  //this is a counter measure when the vac begins to move since it will ahve fewer values.
  
  for (int i = 0; i < MEAN_COUNT; i++) {
    if (dists[i] == NULL)ZeroComp++;
    else sum += dists[i];
    //Serial.println("reading n°" + String(i) + " -value :" + String(dists[i]) );
  }
  if(!stopped)Serial.println("New mean: " + String(sum / (MEAN_COUNT-ZeroComp)));
  if(sum == 0) return 0;
  return (sum / (MEAN_COUNT-ZeroComp))*1.1; //in the mean we only count the real updated values
}



void IRAM_ATTR echo_interruptF() {
  portENTER_CRITICAL_ISR(&mux);
  switch (digitalRead(echoPinF))                     // Test to see if the signal is high or low
  {
    case HIGH:                                      // High so must be the start of the echo pulse
      echo_endF = 0;                                 // Clear the end time
      echo_startF = micros();                        // Save the start time
      break;

    case LOW:                                       // Low so must be the end of hte echo pulse
      echo_endF = micros();                          // Save the end time
      volatile long old = echo_durationF;
      echo_durationF = echo_endF - echo_startF;        // Calculate the pulse duration
      if (echo_durationF / 58 > 500) {
        echo_durationF = old;
      }
      //Serial.println(echo_durationF / 58);
      break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}

void IRAM_ATTR echo_interruptR() {
  portENTER_CRITICAL_ISR(&mux);
  switch (digitalRead(echoPinR))                     // Test to see if the signal is high or low
  {
    case HIGH:                                      // High so must be the start of the echo pulse
      echo_endR = 0;                                 // Clear the end time
      echo_startR = micros();                        // Save the start time
      break;

    case LOW:                                       // Low so must be the end of hte echo pulse
      echo_endR = micros();                          // Save the end time
      volatile long old = echo_durationR;
      echo_durationR = echo_endR - echo_startR;        // Calculate the pulse duration
      if (echo_durationR / 58 > 500) {
        echo_durationR = old;
      }
      //Serial.println(echo_durationR / 58);
      break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}

void radarsInit() {
  pinMode(trigPin, OUTPUT);                           // Trigger pin set to output
  pinMode(echoPinF, INPUT);                            // Echo pin set to input
  pinMode(echoPinR, INPUT);
  attachInterrupt(digitalPinToInterrupt(echoPinF), echo_interruptF, CHANGE);  // Attach interrupt to the sensor echo input
  attachInterrupt(digitalPinToInterrupt(echoPinR), echo_interruptR, CHANGE);  // Attach interrupt to the sensor echo input

  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 50, true);
  timerAlarmEnable(timer);
  Serial.println("Radar OK");
}
