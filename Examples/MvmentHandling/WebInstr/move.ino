#include <analogWrite.h>

#define in1 16
#define in2 17
#define in3 18
#define in4 19

// motor
#define  motA 25
#define  motB 26

#define MaxDeltaSpeed 400
#define Kp 30000
/*int Ki = 0;
  int Kd = 0;*/


#define MinDistToOrient 3 //the min dist to be traveled (cm) for the orientation to take place.

template <class T> const T& sign (const T& a) {//return -1 if negative
  return (a < 0) ? -1 : 1;
}


double CurAngle = 0;//current angle not used
double WanAngle = 0;//wanted angle not used

byte k = 2 ; // angular delta speed constant for rotation

#define MaxDeltaAngle 2 //if delta angle is bigger that offsetAngle we have to orientate, otherwise we compensate while driving
#define DistToStop 3//dist to goal when we stop

//byte MaxDeltaAngle = 20; // For testing

void initMovement() {
  pinMode(motA, OUTPUT);//motor
  pinMode(motB, OUTPUT);//motor
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
}

void handleMovement() {
  syncRadars();//update the positions
  objective.a = atan2((objective.y - vac.y), (objective.x - vac.x));//update the objective angle
  double deltAngle = objective.a - vac.a;//get the angle delta
  if (deltAngle > PI) deltAngle = deltAngle - 2 * PI; //we format the delta angle on a -PI to PI base
  else if (deltAngle < -PI) deltAngle = deltAngle + 2 * PI;

  //if angle is to big we correct the angle
  //orient(deltAngle);
  
  double distToGoal = sqrt(pow(objective.x - vac.x, 2) + pow(objective.y - vac.y, 2));
  
  //if (vac.x > objective.x && vac.y > objective.y) distToGoal = -distToGoal;
  if (abs(deltAngle * 180 / PI) > MaxDeltaAngle && distToGoal > MinDistToOrient && stopped == false) {
    orient(deltAngle);
  }
  else if (abs(distToGoal) > DistToStop && stopped == false) {
    drive(deltAngle, distToGoal);
  }
  else {
    getDist = false;
    reset_dists();
    objective.x = 0;
    objective.y = 0;
    vac.x = 0;
    vac.y = 0;
    stop();
  }


}


void orient(double delta) {//used if orientation is needed (turn left or right) (motors in oposit direction)
  //Serial.println("orient");
  Serial.println("vac a: " + String(vac.a));
  Serial.println("Delta Angle: " + String(delta));
  getDist = false;
  delta > 0 ? turnRight() : turnLeft();//set the direction of orientation
  int comp = 300 + abs(int(delta * k * 180 / PI));//set the speed of rotation

  updatePWM(comp, comp);
}


void drive( double deltAngle, double deltDist) {
  if (!getDist) getDist = true;

  int deltaSpeed = Kp * deltAngle;//PID_control(deltAngle); // we define what part has to be substract

  int pwmL = 1023;// init the pwm
  int pwmR = 1023;

  if (deltAngle > 0)pwmR -= deltaSpeed;
  else if (deltAngle < 0) pwmL += deltaSpeed; //we set the delta speed

  deltDist > 0 ? forward() : back();//set the direction

  if (abs(deltDist) < 10) { //if as we get closer we reduce speed
    pwmL = pwmL * abs(deltDist) / 15;
    pwmR = pwmR * abs(deltDist) / 15;
  }
  updatePWM(pwmL, pwmR);//set the speeds
  Serial.println("Delta dist: " + String(deltDist));
}

/*
  double previousTime, cumError, lastError;
  bool clamp = false; //clamping for the integral part (anti wind-up)

  int PID_control(double a_error) {
  double currentTime = millis();
  double elapsedTime = currentTime - previousTime;

  double rateError = (a_error - lastError) / elapsedTime;//process the derivative part

  if (!clamp) { //if we don't clamp, we process the output with the integral part
    cumError += a_error * elapsedTime;
  }

  int output = Kp * a_error + Ki * cumError + Kd * rateError;

  lastError = a_error;
  previousTime = currentTime;

  if (abs(output) > MaxDeltaSpeed && sign(a_error) == sign(output))clamp = true;
  else clamp = false;

  if (output > MaxDeltaSpeed) output = MaxDeltaSpeed;
  else if (output < -MaxDeltaSpeed) output = -MaxDeltaSpeed;

  return output;
  }

  int sign(double i) {
  if (i > 0)return 1;
  else if (i < 0)return -1;
  else return 0;
  }*/




void updatePWM(int A, int B) {

  analogWrite(motA, A, 1023);
  analogWrite(motB, B, 1023);
}

void turnLeft() {
  //motor L rev
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  //motor R for
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void turnRight() {
  //motor R rev
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  //motor L for
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
}

void back() {
  Serial.println("back");
  //motor L rev
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  //motor R rev
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void forward() {
  Serial.println("for");
  //motor L for
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  //motor R rev
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void stop() {
  //motor L off
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  //motor R off
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);

  stopped = true;
}
