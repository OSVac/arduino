#define BumpPin 4
//#define BumpPinR 39 //not defined yet

volatile int bumpCounter = 0;//used in the interrupt

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR handleBumpInterrupt() {
  portENTER_CRITICAL_ISR(&mux);
  stop();
  //bumpCounter = 1;
  portEXIT_CRITICAL_ISR(&mux);
}

void initBumber() {
  pinMode(BumpPin, INPUT_PULLUP);//left init
  attachInterrupt(digitalPinToInterrupt(BumpPin), handleBumpInterrupt, RISING);
}

void handleBumper() {
  /*if (bumpCounter > 0) {
    
    bumpReverse();
    Serial.println("1");
    
    if (!bumped) {
      //Send to rasp the BUMP !!!
      Serial.println("Bump");
      sendToMaster("B");
      endMvmt();
    }
    bumped = true;
    if (retardMode) {
      //wallTouched_();
      generateNewObjective();
    }
    //reset counter
    portENTER_CRITICAL(&mux);
    bumpCounter = 0;
    portEXIT_CRITICAL(&mux);
  }*/

  if (digitalRead(BumpPin) == HIGH) {//we touch
    if (!bumped) {
      //Send to rasp the BUMP !!!
      sendToMaster("B");
    }
    endMvmt();
    bumped = true;
    bumpReverse();
  }
  else if(bumped == true){
    bumped = false;
    stop();
    endMvmt();
    if (retardMode) generateNewObjective();
  }
}

void bumpReverse() {
  back();
  updatePWM(900, 900);
}
