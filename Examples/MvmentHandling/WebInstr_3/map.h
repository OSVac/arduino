const char MAP_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<html>
<body width="60%" onload="loadData('getmap',init)">

  <canvas id="myCanvas" width="60%" height="150" style="border:1px solid #d3d3d3;">
  Your browser does not support the HTML5 canvas tag.</canvas>

  <script>
    var x = setInterval(function() {loadData('getmap',updateData)}, 500);
    
    function loadData(url, callback){
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
          callback.apply(xhttp);
        }
      };
      xhttp.open('GET', url, true)
      xhttp.send();
    }

    function updateData(){
      var resp = this.responseText;
      var pos = resp.split(",");
      drawBot( parseInt(pos[0],10), parseInt(pos[1],10))
    }

    init_x = 0;
    init_y = 0;
    function init(){
      var resp = this.responseText;
      var pos = resp.split(",");
      init_x = parseInt(pos[0],10);
      init_y = parseInt(pos[1],10);
    }

    var MapWidthCm = 1000;  
    function drawBot(bot_X,bot_Y){

      var wid = window.innerWidth * 0.99;
      var mapUnit = wid / MapWidthCm;

      var c = document.getElementById("myCanvas");
      var ctx = c.getContext("2d");

      ctx.canvas.width  = wid;
      ctx.canvas.height = wid;
      ctx.beginPath();
          //draw the bot
          ctx.arc(wid/2 + (bot_X - init_x )* mapUnit  , wid/2 - ( bot_Y - init_y ) * mapUnit , 19 * mapUnit, 0, 2 * Math.PI);
          ctx.fillStyle = "rgb(120, 120, 255)";
          ctx.fill();
          //draw the lines
          ctx.moveTo(wid/2, 0);
          ctx.lineTo(wid/2, wid);
          ctx.moveTo(0, wid/2);
          ctx.lineTo(wid, wid/2);
          ctx.stroke();
        }
    </script>

    <p><strong>Note:</strong> This is a live map from the OSV robot.</p>
</body>
</html>
)=====";
