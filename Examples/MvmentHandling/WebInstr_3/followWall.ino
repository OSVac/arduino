//pin for the lateral wall detection
#define wallPin 36


//if delta angle is bigger that offsetAngle we have to orientate, otherwise we compensate while driving
#define MaxDeltaAngle 3


int wallDistObj = 0; //dist to the all wanted (it's an it refering to the analog read from the ir sensor)
bool wallDetectInitialized  = false;
bool orienting = false;
double objectiveAngle = 0;

void wallFollowing() {
  /*
     Get The dist
     in function of the dist adapt

     if we hurt a wall
        if something is near (front) we turn 90°
        else if nothing (we are to close from the wall) we adjust (get further from the wall)

     set the speed (has to be quite easy)


     After a full turn we stop. (IDK how to know when he have completed a full turn).

  */
  if (!wallDetectInitialized) { //if we have no objective we can not begin to follow a wall
    initWallFollowing();
  }
  else {
    if (bumped || orienting) {
      if (bumped) {//if just bumped we set the oritentation objectiv
        //we step back to have clearence with the wall
        back();
        updatePWM(500, 500);
        delay(200);
        stop();

        //now we set the angle
        objectiveAngle = vac.a + PI / 12;//we turn left 15°
        if (objectiveAngle > PI) objectiveAngle = objectiveAngle - 2 * PI; //we format the delta angle on a -PI to PI base
        else if (objectiveAngle < -PI) objectiveAngle = objectiveAngle + 2 * PI;

        bumped = false;
        orienting = true;
      }
      else if (abs((vac.a - objectiveAngle) * 180 / PI) > MaxDeltaAngle) { //we orient as the objectiv suggest
        orient(objectiveAngle);
      }
      else {//we end the orienting session
        orienting = false;
        wallDistObj = analogRead(wallPin);
      }
    }
    else {//in this case we drive straight following the wall
      int wallValue = analogRead(wallPin);
      double distError = abs(wallValue - wallDistObj) / 180 * PI; //we simulate an angle error even if it's a distance.
      drive( distError, 100);//we always drive as if there are no front walls
    }
  }


}


bool wallTouched = false;

void initWallFollowing() {//since we are not near a wall we have to find a wall a measure the reference value
  if (!wallTouched && !bumped){
    drive(0, 100); //we drive straight until we hurt a wall
  }
  else if (bumped) {
    wallTouched = true;
    //on set l'objectif de rotation

    //now we set the angle
    objectiveAngle = vac.a + PI / 2;//we turn left 90°
    if (objectiveAngle > PI) objectiveAngle = objectiveAngle - 2 * PI; //we format the delta angle on a -PI to PI base
    else if (objectiveAngle < -PI) objectiveAngle = objectiveAngle + 2 * PI;

    bumped = false;
    orient(objectiveAngle);
  }
  else {
    if (abs((vac.a - objectiveAngle) * 180 / PI) > MaxDeltaAngle) { //objectiv is to far way
      orient(objectiveAngle);
    }
    else {//we read the value and call it initialized
      wallDistObj = analogRead(wallPin);
      wallDetectInitialized = true;
    }
  }
}


void wallTouched_(){
  wallTouched = true;
}
