#include <analogWrite.h>

/*
  #define in1 16
  #define in2 17
  #define in3 18
  #define in4 19

  // motor
  #define  motA 25
  #define  motB 26*/

//for inverted configuration pins
/*#define in3 16
  #define in4 17
  #define in1 18
  #define in2 19
  #define motB 25
  #define motA 26
*/
//for ESAIP configuration pins
#define in3 18
#define in4 19
#define in1 17
#define in2 16
#define motB 26
#define motA 25


double previousTime, cumError, lastError;
//when going straight
#define Kp 12000
#define Ki 1
#define Kd 400

//when orienting
#define oKp 3000
#define oKi 200
#define oKd 100


#define MaxDeltaSpeed 700 //max outputed by the pid function


//old 2
#define k 2 // delta speed constant for rotation


#define MinDistToOrient 3 //the min dist to be traveled (cm) for the orientation to take place.

//old 2
#define MaxDeltaAngle 3 //if delta angle is bigger that offsetAngle we have to orientate, otherwise we compensate while driving
#define DistToStop 2//dist to goal when we stop in cm
#define NearObstacleSpeed 0.7 //describe the speed multiplicator in case we are near an obstacle

void initMovement() {
  pinMode(motA, OUTPUT);//motor
  pinMode(motB, OUTPUT);//motor
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
}

void handleMovement() {
  /*if (followWall) { //if we folow the wall we bypass everything else
    wallFollowing();
    return;
    }*/
  objective.a = atan2((objective.y - vac.y), (objective.x - vac.x));//update obj angle
  double deltAngle = objective.a - vac.a;//get the angle delta
  //Serial.println("Angle :" + String(deltAngle));
  if (deltAngle > PI) deltAngle = deltAngle - 2 * PI; //we format the delta angle on a -PI to PI base
  else if (deltAngle < -PI) deltAngle = deltAngle + 2 * PI;

  double distToGoal = sqrt(pow(objective.x - vac.x, 2) + pow(objective.y - vac.y, 2));

  if (abs(deltAngle * 180 / PI) > MaxDeltaAngle && distToGoal > MinDistToOrient) orient(deltAngle);//orient if angle is too big
  else if (abs(distToGoal) > DistToStop) drive(deltAngle, distToGoal);//otherwise drive if distance to travel is remaining
  else {
    endMvmt();//else stop the vac.
  }
}

void orient(double deltAngle) {//used if orientation is needed (turn left or right) (motors in oposit direction)
  if (getDist) {
    cumError = 0;
    getDist = false;//if we drive we take the distance traveled into account
  }

  //DEBUG("Delta angle: (*1000) " + String(deltAngle * 1000) + "\n");
  getDist = false;//if we orient we don't process the distances
  deltAngle > 0 ? turnRight() : turnLeft();//set the direction of orientation
  //int deltaSpeed = 320 + abs(int(deltAngle * k * 180 / PI));//set the speed of rotation

  int deltaSpeed = abs(PID_control(deltAngle, oKp, oKi, oKd)) + 400;
  if (deltaSpeed > 1023) deltaSpeed = 1023;
  updatePWM(deltaSpeed, deltaSpeed);//apply the speeds
}

void drive( double deltAngle, double deltDist) {
  if (!getDist) {
    stop();
    cumError = 0;
    getDist = true;//if we drive we take the distance traveled into account
    delay(100);
  }
  //int deltaSpeed = Kp * deltAngle; // we define what part has to be substract
  int deltaSpeed = PID_control(deltAngle, Kp, Ki, Kd);
  //DEBUG("PID output: " + String(deltaSpeed));
  int pwmL = 1023;// init the pwm
  int pwmR = 1023;

  if (deltAngle > 0)pwmL -= deltaSpeed;
  else if (deltAngle < 0) pwmR += deltaSpeed; //we set the delta speed

  deltDist < 0 ? back() : forward();//set the direction

  if (abs(deltDist) < 10) { //if as we get closer we reduce speed
    pwmL = pwmL * abs(deltDist) / 5;
    pwmR = pwmR * abs(deltDist) / 5;
  }
  if (nearObstacle) {
    pwmL = pwmL * NearObstacleSpeed + 200;
    pwmR = pwmR * NearObstacleSpeed + 200;
  }
  updatePWM(pwmL, pwmR);//set the speeds
  //DEBUG("Delta angle: (*1000) " + String(deltAngle * 1000) + "\n");
  //DEBUG("Delta dist: " + String(deltDist));
}



bool clamp = false; //clamping for the integral part (anti wind-up)

int PID_control(double a_error, int _Kp, double _Ki, double _Kd) {
  double currentTime = millis();
  double elapsedTime = currentTime - previousTime;
  double rateError = (a_error - lastError) / (elapsedTime / 1000); //process the derivative part

  if (!clamp) { //if we don't clamp, we process the output with the integral part
    cumError += a_error * (elapsedTime / 1000);
  }

  int output = _Kp * a_error + _Ki * cumError + _Kd * rateError;
  //DEBUG("Kp: " + String(_Kp * a_error));
  //DEBUG("Ki: " + String(_Ki * cumError));
  //DEBUG("Kd: " + String(_Kd * rateError));
  lastError = a_error;
  previousTime = currentTime;

  if (abs(_Ki * cumError) > MaxDeltaSpeed && sign(a_error) == sign(output))clamp = true;
  else clamp = false;

  if (output > MaxDeltaSpeed) output = MaxDeltaSpeed;
  else if (output < -MaxDeltaSpeed) output = -MaxDeltaSpeed;

  return output;
}


void endMvmt() {//set to zero the counters
  //vac.x = objective.x;
  //vac.y = objective.y;
  if (!stopped)sendToMaster("N");
  stopped = true;
  getDist = false;
  reset_dists();
  objective.x = vac.x;
  objective.y = vac.y;
  stop();
  cumError = 0;
  lastError = 0;
}


int sign(double i) {
  if (i > 0)return 1;
  else if (i < 0)return -1;
  else return 0;
}




void updatePWM(int A, int B) {
  //DEBUG("Mot L" + String(A));
  //DEBUG("Mot R" + String(B));
  if (max(A, B) == 1023 && sign(A) == sign(B) && !stopped && getDist)fullSpeed = true;
  else fullSpeed = false;
  //Serial.println( String(A) + ":" + String(B));
  analogWrite(motA, abs(A), 1023);
  analogWrite(motB, abs(B), 1023);
}



void turnLeft() {
  //motor L rev
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  //motor R for
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void turnRight() {
  //motor R rev
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  //motor L for
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
}

void forward() {
  //motor L rev
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  //motor R rev
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void back() {
  //motor L for
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  //motor R rev
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void stop() {
  //motor L off
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  //motor R off
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}
