#define NODE_SIZE 10


void handleSerial() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    processResponse(Serial.readString());
  }
}

/*
   IKAKDK

   I - INIT
   A - ANGLE
   D - Destination
*/


void processResponse(String r) {
  CLIENT(r);
  char Code = r.charAt(0);
  int x = r.substring(r.indexOf(',') + 1 , r.lastIndexOf(',')).toInt() * NODE_SIZE;
  int y = r.substring(r.lastIndexOf(',') + 1).toInt() * NODE_SIZE;
  if (Code == 'D') {
    setObjectives(x, y);
    sendToMaster("K");
  }
  else if (Code == 'I') {
    vac.x = x;
    vac.y = y;
    objective.x = vac.x;
    objective.y = vac.y;
    sendToMaster("K");
  }
  else if (Code == 'A') {
    double setAngle = (x / NODE_SIZE + y / NODE_SIZE / pow(10, 8));
    if (setAngle > PI)setAngle = setAngle - 2 * PI; //if angle > PI we make it negativ
    //now we have a [-PI,PI] range
    AngleMapOffset =  setAngle - vac.a;
    sendToMaster("K");
  }
  else if (Code == 'B') {
    endMvmt();
    retardMode = false;
    sendToMaster("K");
  }

  else if (Code == 'r') {//set retard mode ON
    if (x = 20) {
      retardMode = true;
      generateNewObjective();
    }
    else retardMode = false;
    sendToMaster("K");
  }
  else if (Code == 'F') {//set the "follow Wall" mode
    followWall = true;
    sendToMaster("K");
  }
  else if (Code == 'C') {//set the "CLEANING" mode
    setCleaningSpeeds(x,y);
    sendToMaster("K");
  }
  else if (Code == 'R') {//reset
    sendToMaster("K");
    DEBUG("restarting in 2 sec");
    delay(2000);
    ESP.restart();
  }

}


/*
   Error codes :
   K = OK
   B = BUMP
   N = New state
*/
void sendToMaster(String code) {
  DEBUG(code + "," + String(vac.x / NODE_SIZE) + "," + String(vac.y / NODE_SIZE));
  Serial.print(code + ",");
  Serial.print(vac.x / NODE_SIZE , DEC);
  Serial.print(',');
  Serial.println(vac.y / NODE_SIZE , DEC);
}
