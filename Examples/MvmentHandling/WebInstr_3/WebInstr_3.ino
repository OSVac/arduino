/*
  TO DO

  Add second core for autonomous driving.
*/

struct coordinates {
  double x;//position X
  double y;//position Y
  double a;//Angle orientation
};

double AngleMapOffset = 0; // determine the offset relative to the imu and the map (imu + AngleMapOffset) = map
coordinates vac = {0, 0, 0};
coordinates objective = {0, 0, 0};
bool getDist = false;//determines if we have to mesure distances (for example if we turn we don't)
bool stopped = true;

bool nearObstacle = false;
bool bumped = false;
bool fullSpeed = false;
bool followWall = false;

#define WIFI false
bool retardMode = false;

void setup()
{
  Serial.begin(115200);
  stop();
  //DEBUG("Start Init");
  if (WIFI)initServer();
  initMovement();
  initMPU();
  initBumber();
  initRadar();
  setObjectives(0, 0);
  DEBUG("End Init");

  retardMode = true;
  setObjectives(10000,0);
  delay(4000);
  AngleMapOffset =  - vac.a;
  setCleaningSpeeds(1023,1023);
}

void loop1000ms() {

}

void loop200ms() {
  //Handle web client requests
  if (WIFI)clientServer();
}

void loop20ms() {
  //Update Yaw
  getYaw();

  //handle the transmission if a bump occures
  handleBumper();

  //handle movements every 10ms
  if (!stopped) handleMovement() ;
}
void loop50ms() {
  //update the positions
  syncRadars();
}

void loop1ms() {
  handleSerial();
}


void loop()
{
  unsigned long tick  = millis();
  loop1ms();//used for critical event
  //we intruduce a shift to avoid all the loops functions to run at the same time.
  if (tick % 20 == 0)loop20ms();
  if (tick % 50 == 0)loop50ms(); //used for navigation critical type of instruction
  if (tick % 200 == 10)loop200ms(); //used for sub critical or low requency type of task
  if (tick % 1000 == 20)loop1000ms(); // probabely useless
  delay(1);
}
