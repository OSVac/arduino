void setObjectives(int X, int Y) {
  objective.x = X;
  objective.y = Y;
  objective.a = atan2((objective.y - vac.y), (objective.x - vac.x));//in rad
  stopped = false;
  bumped = false;
  //DEBUG("objective X :" + String(objective.x));
  //DEBUG("objective Y :" + String(objective.y));
  //DEBUG("objective Angle:" + String(objective.a * 180.0 / PI));
}

double totalTraveled = 0;
void updateCoord(double DistanceTraveled) {
  if (isnan(DistanceTraveled)) DistanceTraveled = 0;
  if (isnan(vac.a)) vac.a = objective.a;
  vac.x += DistanceTraveled * cos(vac.a);
  vac.y += DistanceTraveled * sin(vac.a);
  totalTraveled += DistanceTraveled;
  //DEBUG("X: " + String(vac.x));
  //DEBUG("Y: " + String(vac.y));
  //DEBUG("Mean added: " + String(DistanceTraveled));
  //DEBUG("Speed: " + String(DistanceTraveled /5) + "m/s");
  //DEBUG("Traveled: " + String(totalTraveled));
  //DEBUG("A: " + String(vac.a * 180.0 / PI));
}


#define goToAngle 30//in degree
void generateNewObjective() {
  double ranAngle = - PI / random(2,6) ; //angle between 90 and 180 degree
  //double ranAngle = - PI / 4 ; //angle between 90 and 180 degree
  double x = cos(ranAngle + vac.a) * 1000 - vac.x;
  double y = sin(ranAngle + vac.a) * 1000 - vac.y;
  setObjectives(x, y);
  DEBUG("New Ojs x: " + String(x) + " - y: " + String(y));
}
