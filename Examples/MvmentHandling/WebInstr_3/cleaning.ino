#include <analogWrite.h>

#define fan 32
#define brush 27


void initCleaning() {
  pinMode(fan, OUTPUT);
  pinMode(brush, OUTPUT);
  analogWrite(fan, 0, 1023);
  analogWrite(brush, 0, 1023);
}




void setCleaningSpeeds( int fspeed, int bspeed) {
  if (abs(fspeed) > 1023) fspeed = 1023;
  if (abs(bspeed) > 1023) bspeed = 1023;
  if (abs(fspeed) < 200) bspeed = 200;

  analogWrite(fan, abs(fspeed), 1023);
  analogWrite(brush, abs(bspeed), 1023);
}
