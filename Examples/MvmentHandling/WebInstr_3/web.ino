#include "WebServer.h"
#include "WiFi.h"
#import "index.h"
#import "map.h"
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#define sendDEBUG true   //define if we print the debug strings 
#define sendCLIENT true  //define if we print the client strings

//WiFi Connection configuration
const char *ssid = "";
const char *password = "";

String Buffer = "";

WebServer server(80);

void initServer() {
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println(WiFi.localIP());

  //Initialize Webserver
  server.on("/", handleRoot);
  server.on("/data", handleData);
  server.on("/post", handlePost);
  server.on("/map", handleMap);
  server.on("/getmap", handleGetMap);
  server.begin();


  ArduinoOTA.setHostname("OSV");
  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      DEBUG("Start updating " + type);
    })
    .onEnd([]() {
      DEBUG("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) DEBUG("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) DEBUG("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) DEBUG("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) DEBUG("Receive Failed");
      else if (error == OTA_END_ERROR) DEBUG("End Failed");
    });

  ArduinoOTA.begin();
}

void clientServer() {
  server.handleClient();
  ArduinoOTA.handle();
}  

void handleRoot() {
  String s = MAIN_page;
  server.send(200, "text/html", s);
}

void handleMap() {
  String s = MAP_page;
  server.send(200,  "text/html", s);
}

void handleGetMap() {
  String resp = String(vac.x) + "," + String(vac.y);
  server.send(200,  "text/plain", resp);
}

void handleData() {
  server.send(200,  "text/plain", Buffer);
  Buffer = "";
}

void handlePost() {
  String New = server.arg("Data");
  server.send(200);
  processResponse(New);
}

void CLIENT(String s){
  if(sendCLIENT)Buffer += "Client: " + s + "\n";
}

void DEBUG(String s){
  if(sendDEBUG)Buffer += "Debug: " + s + "\n";
}
