#define trigPin 33
#define echoPinF 34
#define echoPinR 35

#define TIMER_US 50                                   // 50 uS timer duration 
#define TICK_COUNTS 800    // 50 mS worth of timer ticks


#define SoftwareBump false
#define SoftwareBumpDist 5

#define StandErrorCap 3 //% when the standard error is < 3% we consider the value as reliable

#define DistToSlowDown 25 //the distance at which we slow down

#define DistErrorCoef 1.15 //Multiplicator used to adjust the distance traveled (static measuring error)


volatile long echo_startF = 0;// Records start of echo pulse
volatile long echo_endF = 0;// Records end of echo pulse
volatile long echo_durationF = 0;// Duration - difference between end and start

volatile long echo_startR = 0;// Records start of echo pulse
volatile long echo_endR = 0;// Records end of echo pulse
volatile long echo_durationR = 0;// Duration - difference between end and start

volatile int trigger_time_count = 10;// Count down counter to trigger pulse time
volatile long range_flasher_counter = 0;// Count down counter for flashing distance LED


hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

#define MaxDist 10
#define MaxRelativeDelta 0,1 // in %

double oldRear;
double oldFront;

// --------------------------
// timerIsr() 50uS second interrupt ISR()
// Called every time the hardware timer 1 times out.
// --------------------------
void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&mux);

  static volatile int state = 0;                 // State machine variable

  if (!(--trigger_time_count))                   // Count to 200mS
  { // Time out - Initiate trigger pulse
    trigger_time_count = TICK_COUNTS;           // Reload
    state = 1;                                  // Changing to state 1 initiates a pulse
  }
  switch (state)                                 // State machine handles delivery of trigger pulse
  {
    case 0:                                      // Normal state does nothing
      break;

    case 1:                                      // Initiate pulse
      digitalWrite(trigPin, HIGH);              // Set the trigger output high
      state = 2;                                // and set state to 2
      break;

    case 2:                                      // Complete the pulse
    default:
      digitalWrite(trigPin, LOW);               // Set the trigger output low
      state = 0;                                // and return state to normal 0
      //syncRadars2();
      break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}


#define MEAN_COUNT 10 // the number of distance readings that we keep 
#define MAX_DELTA 5 //
#define MAX_DIST 400 //
#define MIN_DIST 10

double old_r = 0;//get the distances
double old_f = 0;
double r = 0;
double f = 0;
double meanV = 0;

void syncRadars() { //this function define the real traveled distance
  old_f = f;
  old_r = r;
  r = echo_durationR / 58.0;//get the distances
  f = echo_durationF / 58.0;
  if ( f < SoftwareBumpDist && SoftwareBump && !stopped ) {//This is a software bump (for testing)
    bumpReverse();
    stopped = true;
    sendToMaster("B");
    endMvmt();
  }

  nearObstacle = false;//set that we are not near an obstacle
  if (  f < DistToSlowDown && !stopped ) {//if we are, we store it to slow down in the move functions
    nearObstacle = true;
  }


  double delta_f = old_f - f;//get the distance travelled since last reading
  double delta_r = r - old_r;

  if (old_f < MAX_DIST && old_f > MIN_DIST && f < MAX_DIST && f > MIN_DIST && abs(delta_f) < MAX_DELTA) addToList(delta_f);//if the value is in the ranges we add it to the "mean" stack
  if (old_r < MAX_DIST && old_r > MIN_DIST && r < MAX_DIST && r > MIN_DIST && abs(delta_r) < MAX_DELTA) addToList(delta_r);
  if (getDist)updateCoord(getMeanDists());

}


byte indx = 0;
double dists[MEAN_COUNT];

//add the reading to the list
void addToList(double newDist) {
  indx = (indx + 1) % MEAN_COUNT;
  dists[indx] = newDist;
}

void reset_dists() {
  for (int i = 0; i < MEAN_COUNT; i++) {
    dists[i] = NULL;
  }
}


//process the readings and make a mean of the *correct* readings

double getMeanDists() {
  double sum = 0;
  double var = 0; //variance
  int ZeroComp = 0;// this value take track of 'zero' values and don't take it into account for the final count.
  //this is a counter measure when the vac begins to move since it will ahve fewer values.
  for (int i = 0; i < MEAN_COUNT; i++) {
    if (dists[i] == NULL)ZeroComp++;
    else sum += dists[i];
  }
  sum = sum / (MEAN_COUNT - ZeroComp);//process mean

  for (int i = 0; i < MEAN_COUNT; i++) {
    if (dists[i] == !NULL)var += pow(dists[i] - sum, 2);
  }
  double standardError = sqrt(var) / (MEAN_COUNT - ZeroComp) / sum; //%standard error
  
  //apply out of radar range. 
  sum = handleOffline(sum,standardError);

  return sum * DistErrorCoef; //in the mean we only count the real updated values
}

double offlineMeanDists = 0;

double handleOffline(double dist, double standardError) {
  if (fullSpeed && standardError < StandErrorCap) { //if the values are great we count them in the mean
    if (offlineMeanDists == 0)offlineMeanDists = dist;
    double temp = (dist - offlineMeanDists) / offlineMeanDists / 3 ; //process the error to be added to correct
    offlineMeanDists += temp;
  }
  else if (fullSpeed && standardError > 4 * StandErrorCap) {//if we are indeed offline 
    dist = offlineMeanDists;
  }
  return dist;
}


void IRAM_ATTR echo_interruptF() {
  portENTER_CRITICAL_ISR(&mux);
  switch (digitalRead(echoPinF))                     // Test to see if the signal is high or low
  {
    case HIGH:                                      // High so must be the start of the echo pulse
      echo_endF = 0;                                 // Clear the end time
      echo_startF = micros();                        // Save the start time
      break;

    case LOW:                                       // Low so must be the end of hte echo pulse
      echo_endF = micros();                          // Save the end time
      volatile long old = echo_durationF;
      echo_durationF = echo_endF - echo_startF;        // Calculate the pulse duration
      if (echo_durationF / 58 > 500) {
        echo_durationF = old;
      }
      break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}

void IRAM_ATTR echo_interruptR() {
  portENTER_CRITICAL_ISR(&mux);
  switch (digitalRead(echoPinR))                     // Test to see if the signal is high or low
  {
    case HIGH:                                      // High so must be the start of the echo pulse
      echo_endR = 0;                                 // Clear the end time
      echo_startR = micros();                        // Save the start time
      break;

    case LOW:                                       // Low so must be the end of hte echo pulse
      echo_endR = micros();                          // Save the end time
      volatile long old = echo_durationR;
      echo_durationR = echo_endR - echo_startR;        // Calculate the pulse duration
      if (echo_durationR / 58 > 500) {
        echo_durationR = old;
      }
      break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}

void initRadar() {
  pinMode(trigPin, OUTPUT);                           // Trigger pin set to output
  pinMode(echoPinF, INPUT);                            // Echo pin set to input
  pinMode(echoPinR, INPUT);
  attachInterrupt(digitalPinToInterrupt(echoPinF), echo_interruptF, CHANGE);  // Attach interrupt to the sensor echo input
  attachInterrupt(digitalPinToInterrupt(echoPinR), echo_interruptR, CHANGE);  // Attach interrupt to the sensor echo input

  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 50, true);
  timerAlarmEnable(timer);
  DEBUG("Radar OK");
}
