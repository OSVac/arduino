void handleSerial() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    processResponse(Serial.readString());


  }
}

/*
 * IKAKDK
 * 
   I - INIT
   A - ANGLE
   D - Destination
*/


void processResponse(String r) {
  char Code = r.substring(0 , r.indexOf(','));
  int x = r.substring(r.indexOf(',') + 1 , r.lastIndexOf(',')).toInt();
  int y = r.substring(r.lastIndexOf(',') + 1).toInt();

  if(Code == 'D'){
    setObjectives(x,y);
    sendToMaster('K');
  }
  else if(Code == 'I'){
    vac.x = x;
    vac.y = y;
    sendToMaster('K');
  }
  else if(Code == 'A'){
    vac.a = x + y/8 + PI;
    sendToMaster('K');
  }
}




/*
   Error codes :
   K = OK
   B = BUMP
   N = New state
*/
void sendToMaster(char code) {
  Serial.print(code + ',');
  Serial.print(vac.x , DEC);
  Serial.print(',');
  Serial.print(vac.y , DEC);
}
