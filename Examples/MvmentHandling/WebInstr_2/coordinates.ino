void setObjectives(int X, int Y) {
  objective.x = X;
  objective.y = Y;
  objective.a = atan2((objective.y - vac.y), (objective.x - vac.x));//in rad
  stopped = false;
  /*Serial.print("objective X :");
  Serial.println(objective.x);
  Serial.print("objective Y :");
  Serial.println(objective.y);
  Serial.print("objective Angle:");
  Serial.println(objective.a * 180.0 / PI);*/
}

double totalTraveled = 0;
void updateCoord(double DistanceTraveled) {
  vac.x += DistanceTraveled * cos(vac.a);
  vac.y += DistanceTraveled * sin(vac.a);
  totalTraveled += DistanceTraveled;
  //Serial.println("X: " + String(vac.x));
  //Serial.println("Y: " + String(vac.y));
  //Serial.println("Mean added: " + String(DistanceTraveled));
  //Serial.println("Traveled: " + String(totalTraveled));
  //Serial.println("A: " + String(vac.a * 180.0 / PI));
}
