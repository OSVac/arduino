#include <SparkFunMPU9250-DMP.h>

float deltat = 0.0f;
uint32_t Now = 0;   // used to calculate integration interval
uint32_t lastUpdate = 0;

MPU9250_DMP imu;

void initMPU() {
  if (imu.begin() != INV_SUCCESS)
  {
    while (1)
    {
      Serial.println("Unable to communicate with MPU-9250");
      Serial.println("Check connections, and try again.");
      Serial.println();
      delay(5000);
    }
  }

  imu.dmpBegin(DMP_FEATURE_6X_LP_QUAT | // Enable 6-axis quat
               DMP_FEATURE_GYRO_CAL, // Use gyro calibration
               10); // Set DMP FIFO rate to 10 Hz
  // DMP_FEATURE_LP_QUAT can also be used. It uses the
  // accelerometer in low-power mode to estimate quat's.
  // DMP_FEATURE_LP_QUAT and 6X_LP_QUAT are mutually exclusive
  Serial.println("MPU-9250 OK");
}

void getYaw() {
  if ( imu.fifoAvailable() )
  {
    // Use dmpUpdateFifo to update the ax, gx, mx, etc. values
    if ( imu.dmpUpdateFifo() == INV_SUCCESS)
    {
      Now = micros();
      deltat = ((Now - lastUpdate) / 1000000.0f); // set integration time by time elapsed since last filter update
      lastUpdate = Now;
      //compute euler angles
      imu.computeEulerAngles(false);
      vac.a = -imu.yaw + AngleMapOffset;//we update the angle as soon as we can
      if(vac.a > PI) vac.a -= 2*PI;
      if(vac.a < -PI) vac.a += 2*PI;
    }
  }
}
