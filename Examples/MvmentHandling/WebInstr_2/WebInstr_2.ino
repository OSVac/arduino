 /*
   TO DO

   Add second core.
*/


struct coordinates {
  double x;//position X
  double y;//position Y
  double a;//Angle orientation
};

double AngleMapOffset = 0; // determine the offset relative to the imu and the map (imu + AngleMapOffset) = map
coordinates vac = {0, 0, 0};
coordinates objective = {0, 0, 0};
bool getDist = false;//determines if we have to mesure distances (for example if we turn we don't)
bool stopped = true;

void setup()
{
  Serial.begin(115200);
  stop();
  Serial.println("Start Init");
  initServer();
  initMovement();
  initMPU();
  radarsInit();
  setObjectives(0, 0);
  Serial.println("End Init");
}

void loop1000ms() {

}

void loop200ms() {
  //Handle web client requests
  clientServer();
}

void loop50ms() {
  //Update Yaw
  getYaw();

  //handle movements every 10ms
  handleMovement();

  //update the positions
  syncRadars();

  //update the objective angle
  objective.a = atan2((objective.y - vac.y), (objective.x - vac.x));
}

void loop1ms() {
  //handle the transmission if a bump occures
  handleBump();

  handleSerial();
}


void loop()
{
  unsigned long tick  = millis();
  loop1ms();//used for critical event
  //we intruduce a shift to avoid all the loops functions to run at the same time.
  if (tick % 50 == 0)loop50ms(); //used for navigation critical type of instruction
  if (tick % 200 == 10)loop200ms(); //used for sub critical or low requency type of task
  if (tick % 1000 == 20)loop1000ms(); // probabely useless
  delay(1);
}
