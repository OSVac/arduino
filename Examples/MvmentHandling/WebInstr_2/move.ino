#include <analogWrite.h>

#define in1 16
#define in2 17
#define in3 18
#define in4 19

// motor
#define  motA 25
#define  motB 26

#define MaxDeltaSpeed 400

#define Kp 30000
/*#define Ki 0
  #define Kd 0*/

#define MinDistToOrient 3 //the min dist to be traveled (cm) for the orientation to take place.

#define k 2 // angular delta speed constant for rotation

#define MaxDeltaAngle 2 //if delta angle is bigger that offsetAngle we have to orientate, otherwise we compensate while driving
#define DistToStop 3//dist to goal when we stop

void initMovement() {
  pinMode(motA, OUTPUT);//motor
  pinMode(motB, OUTPUT);//motor
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
}

void handleMovement() {
  double deltAngle = objective.a - vac.a;//get the angle delta
  if (deltAngle > PI) deltAngle = deltAngle - 2 * PI; //we format the delta angle on a -PI to PI base
  else if (deltAngle < -PI) deltAngle = deltAngle + 2 * PI;

  double distToGoal = sqrt(pow(objective.x - vac.x, 2) + pow(objective.y - vac.y, 2));

  if (abs(deltAngle * 180 / PI) > MaxDeltaAngle && distToGoal > MinDistToOrient && stopped == false) orient(deltAngle);//orient if angle is too big
  else if (abs(distToGoal) > DistToStop && stopped == false) drive(deltAngle, distToGoal);//otherwise drive if distance to travel is remaining
  else{
    endMvmt();//else stop the vac. 
    sendToMaster('N');
  }
}

void orient(double delta) {//used if orientation is needed (turn left or right) (motors in oposit direction)
  getDist = false;//if we orient we don't process the distances 
  delta > 0 ? turnRight() : turnLeft();//set the direction of orientation
  int comp = 300 + abs(int(delta * k * 180 / PI));//set the speed of rotation
  updatePWM(comp, comp);//apply the speeds
}

void drive( double deltAngle, double deltDist) {
  if (!getDist) getDist = true;//if we drive we take the distance travveled into account

  int deltaSpeed = Kp * deltAngle;//PID_control(deltAngle); // we define what part has to be substract
  int pwmL = 1023;// init the pwm
  int pwmR = 1023;
  
  if (deltAngle > 0)pwmR -= deltaSpeed;
  else if (deltAngle < 0) pwmL += deltaSpeed; //we set the delta speed

  deltDist > 0 ? forward() : back();//set the direction

  if (abs(deltDist) < 10) { //if as we get closer we reduce speed
    pwmL = pwmL * abs(deltDist) / 15;
    pwmR = pwmR * abs(deltDist) / 15;
  }
  updatePWM(pwmL, pwmR);//set the speeds
  Serial.println("Delta dist: " + String(deltDist));
}

void endMvmt() {//set to zero the counters
  stopped = true;
  getDist = false;
  reset_dists();
  objective.x = 0;
  objective.y = 0;
  vac.x = 0;
  vac.y = 0;
  stop();
}

void updatePWM(int A, int B) {
  analogWrite(motA, A, 1023);
  analogWrite(motB, B, 1023);
}

void turnLeft() {
  //motor L rev
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  //motor R for
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void turnRight() {
  //motor R rev
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  //motor L for
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
}

void back() {
  Serial.println("back");
  //motor L rev
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  //motor R rev
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void forward() {
  Serial.println("for");
  //motor L for
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  //motor R rev
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void stop() {
  //motor L off
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  //motor R off
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}
