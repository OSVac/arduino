//This code is used with an esp8266 to control the server fan for the vaccuum. 
//It need a classic PWM signal > 20%
//That's all 
//The PWM pin is linked to the yellow wire (IMPORTANT)
// and the ground must be the linked as well. 


//Initializing LED Pin
int fan_pin = D5;

void setup() {
  //Declaring LED pin as output
  Serial.begin(115200);
  pinMode(fan_pin, OUTPUT);
}


void loop() {
  analogWrite(fan_pin, 190);//best = 187 (190 stable)
  delay(1000);
}
