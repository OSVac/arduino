/*
 * Rui Santos 
 * Complete Project Details https://randomnerdtutorials.com
*/

int sensorPin=4;
int analog=A1;
boolean val =0;

void setup(){
  pinMode(ledPin, OUTPUT);
  pinMode(sensorPin, INPUT);
  Serial.begin (115200);
}
  
void loop (){
  val =digitalRead(sensorPin);
  //Serial.print("Analog =");
  //Serial.println(analogRead(analog));
  //Serial.print("Pic =");
  //Serial.println (val);
  // when the sensor detects a signal above the threshold value, LED flashes
  if (val==HIGH) {
    digitalWrite(LED_BUILTIN, HIGH);
  }
  else {
    digitalWrite(LED_BUILTIN, LOW);
  }
}
