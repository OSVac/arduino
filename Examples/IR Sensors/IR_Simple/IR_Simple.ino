const int IRSensor = 5;

void setup() {
  // initialize the digital pin as an output.
  //Pin 4 is connected to the output of IR sensor
  Serial.begin(115200);
  pinMode(IRSensor, INPUT_PULLUP);
}

void loop() {
  if (digitalRead(IRSensor) == HIGH) //Check the sensor output
  {
    Serial.println("Contact imminent");
  }
  else
  {
    Serial.println("No stress");
  }
  delay(5);
}
