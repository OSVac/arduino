const byte interruptPin = 18;
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR handleInterrupt() {
  portENTER_CRITICAL_ISR(&mux);
  interruptCounter = 1;
  portEXIT_CRITICAL_ISR(&mux);
}

void setup() {

  Serial.begin(115200);
  Serial.println("Monitoring interrupts: ");
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, FALLING);

}

void loop() {

  if (interruptCounter > 0) {
    if (digitalRead(interruptPin)) {
      portENTER_CRITICAL(&mux);
      interruptCounter--;
      portEXIT_CRITICAL(&mux);
      Serial.print("An interrupt has occurred. state: ");
      Serial.println(HIGH);
    }
    else {
      Serial.print("An interrupt has occurred. state: ");
      Serial.println(LOW);
    }


    //numberOfInterrupts++;
  }
}
