#include "MPU9250.h"
MPU9250 IMU(Wire, 0x68);

struct EulerAngles
{
  double roll, pitch, yaw;
};

int status;

int16_t AcX, AcY, AcZ, GyX, GyY, GyZ, MaX, MaY, MaZ;



void setup() {
  Serial.begin(115200);
  while (!Serial) {}

  // start communication with IMU
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while (1) {}
  }
    // setting the accelerometer full scale range to +/-8G 
  IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
  // setting the gyroscope full scale range to +/-500 deg/s
  IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
  // setting DLPF bandwidth to 20 Hz
  IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
  // setting SRD to 19 for a 50 Hz update rate
  IMU.setSrd(19);
}

void loop()
{
  IMU.readSensor();
  AcX = IMU.getAccelX_mss();
  AcY = IMU.getAccelY_mss();
  AcZ = IMU.getAccelZ_mss();
  GyX = IMU.getGyroX_rads();
  GyY = IMU.getGyroY_rads();
  GyZ = IMU.getGyroZ_rads();
  MaX = IMU.getMagX_uT();
  MaY = IMU.getMagY_uT();
  MaZ = IMU.getMagZ_uT();

  float pitch = 180 * atan (AcX / sqrt(AcY * AcY + AcZ * AcZ)) / M_PI;
  float roll = 180 * atan (AcY / sqrt(AcX * AcX + AcZ * AcZ)) / M_PI;
  float yaw = 180 * atan ( sqrt(AcX * AcX + AcY * AcY) / AcZ ) / M_PI;
  
  Serial.print("Roll: ");
  Serial.println(roll);
  Serial.print("Pitch: ");
  Serial.println(pitch);
  Serial.print("Yaw: ");
  Serial.println(yaw);
  Serial.println(".");

  delay(16);
}
