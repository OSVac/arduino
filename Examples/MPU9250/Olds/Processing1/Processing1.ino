// MPU-6050 Short Example Sketch
// By Arduino User JohnChi
// August 17, 2014
// Public Domain

//Modified by Pravat Kumar


#include "MPU9250.h"
MPU9250 IMU(Wire, 0x68);
int status;

int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
float r, b;
long t = 0;
long tdiff;
float dt, anv, angle = 0, an;


void setup() {
  Serial.begin(115200);
  while (!Serial) {}

  // start communication with IMU
  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while (1) {}
  }
}

void loop()
{
  IMU.readSensor();
  AcX = IMU.getAccelX_mss();
  AcY = IMU.getAccelY_mss();
  AcZ = IMU.getAccelZ_mss();
  GyX = IMU.getGyroX_rads();
  GyY = IMU.getGyroY_rads();
  GyZ = IMU.getGyroZ_rads();

  b = float(AcY) / sqrt((double(AcZ) * double(AcZ)) + (double(AcX) * double(AcX)));;
  r = (atan(b) * 180) / 3.1416; //radian to degree

  tdiff = millis() - t;     //getting time difference in milli sec
  dt = float(tdiff) / 1000; //converting to seconds
  anv = (float(GyX) + 200) / 131; //getting instantaneous angular velocity
  //200 was the gyroscope offset value for me
  //131 is the LBS sensitivity(Check data sheet)
  angle = angle + (anv * dt); //integrating to find angle from gyroscope

  //Complementary filter
  //Combines Gyroscope & Accelerometer data
  an = ((0.8) * (an + (anv * dt))) + (r * 0.20);

  /*
     Now that all data are ready its time to send them
     Three readings are sent each time & to distinguish one data from other
     i am sending stop bytes (like '*', '@', '.').
  */
  Serial.print(int(r));
  Serial.print("*");
  Serial.print(int(angle));
  Serial.print("@");
  Serial.print(int(an));
  Serial.print(".");

  t = millis();
  delay(100);
}
