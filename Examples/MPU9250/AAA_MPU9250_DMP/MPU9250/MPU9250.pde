import processing.serial.*;
Serial port;

String data="", roll="", pitch="", yaw="";
int index=0, index2=0;

int rec1x=25,rec1y=80;
int rec2x=rec1x+275,rec2y=rec1y;
int rec3x=rec2x+275,rec3y=rec1y;

float x1,y1,x2,y2,cx,cy,a,b;
float ang1=0,ang2=0,ang3=0,angle1,angle2,angle3;

PFont font1,font2,font3;

void setup()
{
 size(900,400);
 smooth();
 port=new Serial(this,  "/dev/cu.usbserial-144130", 115200);
 port.bufferUntil('.');
 font1=loadFont("Copperplate-48.vlw");
 font2=loadFont("Baskerville-80.vlw");
 font3=loadFont("AppleSymbols-80.vlw"); 
}

void draw()
{
  background(50);
  textFont(font2,60);
  fill(40,200,255);
  text("mpu-9250",300,50);
  fill(1,255,1);
  noStroke();

  
  ang1=float(roll);
  ang2=float(pitch);
  ang3=float(yaw);
                                          //roll
  noStroke();
  fill(50,10,250);
  rect(rec1x,rec1y,250,20);
  textFont(font1,22);
  fill(255);
  text("Roll",125,98);
  noStroke();
  fill(50,20,200,80);
  rect(rec1x,rec1y+20,250,250);
  noStroke();
  fill(0);
  ellipse(rec1x+(250/2),rec1y+20+(250/2),240,240);
                                      //roll_line
  cx=rec1x+(250/2);
  cy=rec1y+20+(250/2);
  angle1=radians(ang1);
  a=120*cos(angle1);
  b=120*sin(angle1);
  x1=cx-a;
  y1=cy-b;
  x2=cx+a;
  y2=cy+b;
  stroke(255,0,0);
  strokeWeight(2);
  line(x1,y1,x2,y2);
  fill(0);
  noStroke();
  ellipse(cx,cy,80,80);
  textFont(font3,70);
  fill(40,200,255);
  text(abs(int(ang1)),cx-30,cy+22);
                                             //pitch
  noStroke();
  fill(50,10,250);
  rect(rec2x,rec2y,250,20);
  textFont(font1,22);
  fill(255);
  text("Pitch",395,98);
  noStroke();
  fill(50,20,200,80);
  rect(rec2x,rec2y+20,250,250);
  fill(0);
  ellipse(rec2x+(250/2),rec2y+20+(250/2),240,240);
                                          //pitch_line
  cx=rec2x+(250/2);
  cy=rec2y+20+(250/2);
  angle2=radians(ang2);
  a=120*cos(angle2);
  b=120*sin(angle2);
  x1=cx-a;
  y1=cy-b;
  x2=cx+a;
  y2=cy+b;
  stroke(2,2,255);
  strokeWeight(2);
  line(x1,y1,x2,y2);
  fill(0);
  noStroke();
  ellipse(cx,cy,80,80);
  textFont(font3,70);
  fill(40,200,255);
  text(abs(int(ang2)),cx-30,cy+22);
                                                  //yaw
  noStroke();
  fill(50,10,250);
  rect(rec3x,rec3y,250,20);
  textFont(font1,22);
  fill(255);
  text("Yaw",680,98);
  noStroke();
  fill(50,20,200,80);
  rect(rec3x,rec3y+20,250,250);
  fill(0);
  ellipse(rec3x+(250/2),rec3y+20+(250/2),240,240);
                                           //yaw_line
  cx=rec3x+(250/2);
  cy=rec3y+20+(250/2);
  angle3=radians(ang3);
  a=120*cos(angle3);
  b=120*sin(angle3);
  x1=cx-a;
  y1=cy-b;
  x2=cx+a;
  y2=cy+b;
  stroke(0,255,0);
  strokeWeight(2);
  line(x1,y1,x2,y2);
  fill(0);
  noStroke();
  ellipse(cx,cy,80,80);
  textFont(font3,70);
  fill(40,200,255);
  text(abs(int(ang3)),cx-30,cy+22);
 
}
void serialEvent(Serial port)
{
  data= port.readStringUntil('.');
  data=data.substring(0,data.length()-1);
  index= data.indexOf("*");
  index2=data.indexOf("@");
  pitch= data.substring(0,index);
  roll= data.substring(index+1,index2);
  yaw=data.substring(index2+1,data.length());
}
