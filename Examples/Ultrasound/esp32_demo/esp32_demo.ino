
/* *********************************************
    Subsonic echo sensor paramters declaration
 * ********************************************* */
#define ECHO 27          // Echo Pin
#define TRIG 26          // Trigger Pin

#define MEAS_COUNT 1   // 1 cycleの測定数 
double Duration = 0;    //受信した間隔
double Distance = 0;    //距離


/* *************************************
    Measuring distance
 * ************************************* */
double measureDistance() {

  int measCnt = 0;
  double dista = 0;
  Distance = 0;

  for (int i = 0; i < MEAS_COUNT; i++) {
    digitalWrite(TRIG, LOW);
    delayMicroseconds(2);

    digitalWrite(TRIG, HIGH);       //超音波を出力
    delayMicroseconds(10);
    digitalWrite(TRIG, LOW);

    Duration = pulseIn(ECHO, HIGH); //センサからの入力

    if (Duration > 0) {
      //dista = (Duration / 2) * 340 * 100 / 1000000;
      Distance += (Duration /58); // 音速を340m/sで固定。気温補正なし
      measCnt += 1;
    }
    delay(50);
  }
  //return dista;
  return (double)(Distance / measCnt);  // defineで定義した回数分測定し、有効な値(> 0)を得た時の平均値を返す
}


/* ****************
   setup
 ****************** */
void setup() {
  Serial.begin (115200);
  delay(500);

  // Subsonic echo sensor
  pinMode(ECHO, INPUT);
  pinMode(TRIG, OUTPUT);
}

/* ******************
   main func
 * ****************** */
void loop() {

  delay(300);
  double dist;
  // notify changed value

  dist = measureDistance();

  Serial.print("dist= ");
  Serial.println(dist);
}
