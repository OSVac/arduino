#include <SparkFunMPU9250-DMP.h>


float deltat = 0.0f;
uint32_t Now = 0;   // used to calculate integration interval
uint32_t lastUpdate = 0;
double lastYaw = 0;
double Angle = 90;
double k = 4;

MPU9250_DMP imu;


// motor
int enA = 12; //D3
int enB = 14; //D5
const int timeout = 20;


void setup()
{
  Serial.begin(115200);
  pinMode(enA, OUTPUT);//motor
  pinMode(enB, OUTPUT);//motor



  Serial.println("Start");
  // Call imu.begin() to verify communication and initialize
  if (imu.begin() != INV_SUCCESS)
  {
    while (1)
    {
      Serial.println("Unable to communicate with MPU-9250");
      Serial.println("Check connections, and try again.");
      Serial.println();
      delay(5000);
    }
  }

  imu.dmpBegin(DMP_FEATURE_6X_LP_QUAT | // Enable 6-axis quat
               DMP_FEATURE_GYRO_CAL, // Use gyro calibration
               10); // Set DMP FIFO rate to 10 Hz
  // DMP_FEATURE_LP_QUAT can also be used. It uses the
  // accelerometer in low-power mode to estimate quat's.
  // DMP_FEATURE_LP_QUAT and 6X_LP_QUAT are mutually exclusive
  Serial.println("loop");
}

void loop()
{
  // Check for new data in the FIFO
  if ( imu.fifoAvailable() )
  {
    // Use dmpUpdateFifo to update the ax, gx, mx, etc. values
    if ( imu.dmpUpdateFifo() == INV_SUCCESS)
    {
      Now = micros();
      deltat = ((Now - lastUpdate) / 1000000.0f); // set integration time by time elapsed since last filter update
      lastUpdate = Now;
      // computeEulerAngles can be used -- after updating the
      // quaternion values -- to estimate roll, pitch, and yaw
      imu.computeEulerAngles();
      handleMotors(Angle);
      //printIMUData();
      //printProc();
    }
  }
}

void handleMotors(double ang) {
  double deltAngle = ang - imu.yaw;
  if(deltAngle>180) deltAngle -= 360;
  else if(deltAngle<-180) deltAngle += 360;
  int comp = int(deltAngle * k);
  int pwmL =  map(1023 + comp, min(0,min(1023 + comp, 1023 - comp)), max(1023 + comp, 1023 - comp), 0, 1023);
  int pwmR =  map(1023 - comp, min(0,min(1023 + comp, 1023 - comp)), max(1023 + comp, 1023 - comp), 0, 1023);
  Serial.println("Comp: " + String(comp));
  Serial.println("pwmL: " + String(pwmL));
  Serial.println("pwmR: " + String(pwmR));
  analogWrite(enA, pwmL);
  analogWrite(enB, pwmR);
}

void printProc(void) {
  // Send the data to the serial port
  Serial.print(int(imu.roll));
  Serial.print("*");
  Serial.print(int(imu.pitch));
  Serial.print("@");
  Serial.print(int(imu.yaw));
  Serial.print(".");
}



void printIMUData(void)
{
  Serial.println("R/P/Y: " + String(imu.roll) + ", " + String(imu.pitch) + ", " + String(imu.yaw));
  Serial.println("Time: " + String(imu.time) + " ms");
  Serial.println();
}
