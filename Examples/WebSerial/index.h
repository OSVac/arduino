const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<h1>OSV WebSerial debug</h1>

<textarea id='data' rows="50" cols="150" readonly></textarea>

<br>
Send to Serial: <input type="text" name="toSerial" id='toSerial' onkeydown="KeyPre()">
<input type="checkbox" id="autosScroll" name="autosScroll" checked>
<label for="autosScroll">Auto Scroll</label>
<input type="checkbox" id="Time" name="Time"checked>
<label for="Time">TimeStamp</label>
<button type="button" onclick="clearData()">Clear</button> 
<script>
  var History = [];
  var index = 0;
  var subHist =0;

  var x = setInterval(function() {loadData('data',updateData)}, 500);
  function loadData(url, callback){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        callback.apply(xhttp);
      }
    };
    xhttp.open('GET', url, true)
    xhttp.send();
  }

  function updateData(){
    var currentdate = new Date(); 
    var textarea = document.getElementById('data');
    if( document.getElementById("Time").checked && this.responseText != "") textarea.value += currentdate.getHours() + ":" + currentdate.getMinutes() + ":"  + currentdate.getSeconds()+ " --> " + "\n";
    textarea.value += this.responseText;
    if( document.getElementById("autosScroll").checked && this.responseText != "") textarea.scrollTop = textarea.scrollHeight;
  }

  function KeyPre(key){
    key = key || window.event;
    if (key.keyCode == '13') postData();//if enter
    if (key.keyCode == '38') {
      subHist -=1;
      if(subHist < 0) subHist=0;
      document.getElementById('toSerial').value = History[subHist];
        // up arrow
    }
    else if (key.keyCode == '40') {
      subHist +=1;
      if(subHist >= index){
        subHist = index ;
        document.getElementById('toSerial').value = "";
      }
      else document.getElementById('toSerial').value = History[subHist];
        // down arrow
    }
}

function clearData(){
  document.getElementById('data').value = "";
}

function postData(){
  subHist = 0;
  var val = document.getElementById('toSerial').value;
  History[index] = val;
  index = (index + 1)%20;
  subHist = index;
  $.post("post",
  {
    Data: val
  },
  function(data, status){
    console.log("Data: " + data + "\nStatus: " + status);
  },"text");
  document.getElementById('toSerial').value = "";
}
</script>
)=====";
