/*
   ESP8266 SPIFFS HTML Web Page with JPEG, PNG Image
   https://circuits4you.com
*/

#include "WiFi.h"
#import "index.h"
#include "WebServer.h"

//WiFi Connection configuration
const char *ssid = "Modem_56H";
const char *password = "Wooken422";

String Buffer = "";

WebServer server(80);

void handleRoot() {
  String s = MAIN_page;
  server.send(200, "text/html", s);
}

void handleData() {
  server.send(200,  "text/plain", Buffer);
  Buffer = "";
}

void handlePost() {
  String NewX = server.arg("Data");
  Buffer += "Client: " + NewX + "\n";
  //Send to the treatment function
  server.send(200);
}

void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();

  //Connect to wifi Network
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP

  //Initialize Webserver
  server.on("/", handleRoot);
  server.on("/data", handleData);
  server.on("/post", handlePost);
  server.begin();
}

void loop() {
  server.handleClient();
}



void DEBUG(String s){
  Buffer += "Debug: " + s + "\n";
}
