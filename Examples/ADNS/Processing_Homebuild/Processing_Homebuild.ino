#include "ADNS3050.h"
#include <SPI.h>

#define FRAMELENGTH 361

int frame[FRAMELENGTH];


void setup() {
  startup();

}


void loop()
{
  unsigned int s;
  int input;

  readFrame();

  if ( Serial.available() )
  {
    input = Serial.read();
    switch ( input )
    {
      case 'f':
        Serial.println("Frame capture.");
        readFrame();
        Serial.println("Done.");
        break;
      case 'd':
        for ( input = 0; input < FRAMELENGTH; input++ ) //Reusing 'input' here
          //Serial.print( 'c' );
          Serial.print( (char)frame[input] );
        Serial.println( (char)255 );
        break;
      case 't':
        for ( input = 0; input < FRAMELENGTH; input++ ) { //Reusing 'input' here
          if (input % 19 == 0)Serial.println();
          Serial.print( (byte) frame[input]);
          Serial.print( " ");
        }
        Serial.println( (byte)255 );
        break;
    }
    Serial.flush();
  }
}

void readFrame()
{


  //Write(PIX_GRAB, 0x00);
  byte val = 0;
  int pos = 0;
  int uBound = 362;

  unsigned long timeout = millis() + 1000;

  //There are three terminating conditions from the following loop:
  //1. Receive the start-of-field indicator after reading in some data (Success!)
  //2. Pos overflows the upper bound of the array (Bad! Might happen if we miss the start-of-field marker for some reason.)
  //3. The loop runs for more than one second (Really bad! We're not talking to the chip properly.)
  for (int j=0; j < 361; j++) {
    val = Read(PIX_GRAB);
    frame[j] = val;
    if (val > 127) {
      //Serial.println("Continue");
      j--;
    }
    //frame[j] = val & 127;
    
  }


  /*while ( millis() < timeout && pos < 362)
    {
    val = Read(PIX_GRAB);
    //Only bother with the next bit if the pixel data is valid.
    //if ( !(val & 128) )continue;

    //If we encounter a start-of-field indicator, and the cursor isn't at the first pixel,
    //then stop. ('Cause the last pixel was the end of the frame.)
    //if ( ( val & 128 ) &&  ( pos != 0) )break;

    //frame[pos] = val & 127;
    frame[pos] = val;
    pos++;
    }*/

}
