#include <Wire.h>
#include <MechaQMC5883.h> //this library is modified to take short as input. (replace uint16_t by short) 
// it will shift the bits (0-65536 => -32768-32767)

MechaQMC5883 qmc;

void setup() {
  Wire.begin();
  Serial.begin(115200);
  qmc.init();
  qmc.setMode(Mode_Continuous,ODR_200Hz,RNG_2G,OSR_256);
}



void loop() {
  float angle = compass();
  if(angle != -1){
    Serial.print(" angle = ");
    Serial.println(angle); 
  }
 
  delay(20);
}



float decimal(float a){//is used to keep the decimal number even when we cast the float to int
  return a - (int)a;
}


float lin[] = {0,0,0,0,0,0,0,0,0,0};
short incLin = 0;
bool edge = false; // define if we are on the edge of the circle ( around 360° ± 40°)

float compass(){
  short x,y,z;//Variables for the read
  float a;//azimuth
  float avg = 0;//average azimuth
  qmc.read(&x,&y,&z, &a);//read the values from sensor
  
  if( a > 320 || a < 40 || edge) {//if we are on the edge we shift the angle
    a = ((int)a+180)%360 + decimal(a);
    if(!edge){
      edge = true;
      for(int i=0; i< incLin ;i++){
        lin[i] = ((int)lin[i]+180)%360 + decimal(lin[i]);
      }
    }
  }
  lin[incLin] = a;//we store the value
  incLin++;//increment the cursor

  if(incLin>9){//if we have 10 new values, we count them
    incLin = 0;//since we ended the array we reset the cursor
    float max = 0; //we define a min and max to ba able to remove extreem points
    float min = 360;
    for(int i=0; i <= 9; i ++){
      avg += lin[i];//we add avery values 
      if(lin[i]>max)max = lin[i];//define the min and max value
      if(lin[i]<min)min = lin[i];
    }
    avg = (avg-(min + max))/8;//we remove the min and max and make the average
    
    if(edge){//remove angle shift
      avg = ((int)avg+180)%360 + decimal(avg);
      edge = false;
    }
    return avg;
  }
  return -1;
}
