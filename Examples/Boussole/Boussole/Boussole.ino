#include <Wire.h>
#include <MechaQMC5883.h> //this library is modified to take short as input. (replace uint16_t by short) 
// it will shift the bits (0-65536 => -32768-32767)

MechaQMC5883 qmc;

void setup() {
  Wire.begin();
  Serial.begin(115200);
  qmc.init();
  qmc.setMode(Mode_Continuous,ODR_200Hz,RNG_2G,OSR_256);
}


bool DEBUG = true;

void loop() {
  short x,y,z;
  float a;
  qmc.read(&x,&y,&z, &a);

  if(DEBUG){
    //Serial.print("x: ");
    //Serial.print(x);
    //Serial.print(" y: ");
    //Serial.print(y);
    //Serial.print(" z: ");
    //Serial.print(z);
    Serial.print(" a: ");
    Serial.print(a);  
    Serial.println();
  }
 

  delay(60);
}
