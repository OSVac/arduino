
const byte Motor_pin = 5;
const byte interruptPin = 4;
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;

int _speed = 100;
int rpm = 0;
int objectif = 150;
int motorOffSet = 1;

void setup() {

  Serial.begin(115200);
  Serial.println("Monitoring interrupts: ");
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, CHANGE);

}

unsigned long timeout = 0;
void loop() {


  if (timeout < millis()) {
    timeout = millis() + 20;

    int Count = interruptCounter;
    Serial.println(_speed);
    interruptCounter = 0;
    int rpm = Count*37.5/2;
    Serial.print("rpm : ");
    Serial.println(rpm);
    if(rpm > objectif + 3){
      _speed -= motorOffSet;
    }
    else if(rpm < objectif - 3){
      _speed += motorOffSet;
    }
    analogWrite(Motor_pin,_speed);
  }
}



unsigned long LastInterrupt = 0;
void handleInterrupt() {
  if(LastInterrupt + 1 >=  millis())return;
  LastInterrupt = millis();
  interruptCounter += 1;
}
