//esp8266
const byte interruptPin = 4;
volatile int interruptCounter = 0;
int numberOfInterrupts = 0;



void setup() {

  Serial.begin(115200);
  Serial.println("Monitoring interrupts: ");
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, CHANGE);

}

unsigned long timeout = 0;
void loop() {


  if (timeout < millis()) {
    timeout = millis() + 1000;

    int Count = interruptCounter;
    Serial.println(interruptCounter);
    interruptCounter = 0;
    int rpm = Count*1.5;
    Serial.print("rpm : ");
    Serial.println(rpm);
    }
}



unsigned long LastInterrupt = 0;
void handleInterrupt() {
  if(LastInterrupt + 1 >=  millis())return;
  LastInterrupt = millis();
  interruptCounter += 1;
}
