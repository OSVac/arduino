// Bounce.pde
// -*- mode: C++ -*-
//
// Make a single stepper bounce from one limit to another
//
// Copyright (C) 2012 Mike McCauley
// $Id: Random.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $

#include <AccelStepper.h>

// Motor pin definitions:
#define motorPin1  14      // IN1 on the ULN2003 driver
#define motorPin2  12     // IN2 on the ULN2003 driver
#define motorPin3  13     // IN3 on the ULN2003 driver
#define motorPin4  4    // IN4 on the ULN2003 driver

// Define the AccelStepper interface type; 4 wire motor in half step mode:
#define MotorInterfaceType 8

// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper library with 28BYJ-48 stepper motor:
AccelStepper stepper = AccelStepper(MotorInterfaceType, motorPin1, motorPin3, motorPin2, motorPin4);

void setup()
{
  // Change these to suit your stepper if you want
  stepper.setMaxSpeed(500);
  stepper.setAcceleration(8000);
  stepper.moveTo(1024 );
}

void loop()
{
  // If at the end of travel go to the other end
  if (stepper.distanceToGo() == 0) {
    stepper.disableOutputs();
    delay(2000);
    stepper.enableOutputs();
    stepper.moveTo(-stepper.currentPosition());
    
  }

  stepper.run();

}
