#include <Wire.h>
#include <MechaQMC5883.h> //this library is modified to take short as input. (replace uint16_t by short) 
// it will shift the bits (0-65536 => -32768-32767)


#include <AccelStepper.h>

// Motor pin definitions:
#define motorPin1  14      // IN1 on the ULN2003 driver
#define motorPin2  12     // IN2 on the ULN2003 driver
#define motorPin3  13     // IN3 on the ULN2003 driver
#define motorPin4  16    // IN4 on the ULN2003 driver

// Define the AccelStepper interface type; 4 wire motor in half step mode:
#define MotorInterfaceType 8

// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper library with 28BYJ-48 stepper motor:
AccelStepper stepper = AccelStepper(MotorInterfaceType, motorPin1, motorPin3, motorPin2, motorPin4);


MechaQMC5883 qmc;

void setup() {
  Wire.begin();
  Serial.begin(115200);
  qmc.init();
  qmc.setMode(Mode_Continuous,ODR_200Hz,RNG_2G,OSR_256);

  // Change these to suit your stepper if you want
  stepper.setMaxSpeed(1024);
  stepper.setAcceleration(2000);
  stepper.moveTo(500);
}


float wanted = 160;//the angle we want to be 
float delta = 0;
void loop() {
  if(millis()%20==0){
    float angle = compass();
    if(angle != -1){//dedans 
      delta = (angle - wanted)*4096/360;
      //if(delta > 2048)delta = 4096-delta;
      Serial.print(" delta = ");
      Serial.println(delta);    
      if(delta > 24 || delta < -24)stepper.move((int)(-delta)%4096);
      else stepper.moveTo(stepper.currentPosition());
    }
  }
  
 stepper.run();
 //20ms
}



float decimal(float a){//is used to keep the decimal number even when we cast the float to int
  return a - (int)a;
}


float lin[] = {0,0,0,0,0,0,0,0,0,0};
short incLin = 0;
bool edge = false; // define if we are on the edge of the circle ( around 360° ± 40°)

float compass(){
  short x,y,z;//Variables for the read
  float a;//azimuth
  float avg = 0;//average azimuth
  qmc.read(&x,&y,&z, &a);//read the values from sensor
  
  if( a > 320 || a < 40 || edge) {//if we are on the edge we shift the angle
    a = ((int)a+180)%360 + decimal(a);
    if(!edge){
      edge = true;
      for(int i=0; i< incLin ;i++){
        lin[i] = ((int)lin[i]+180)%360 + decimal(lin[i]);
      }
    }
  }
  lin[incLin] = a;//we store the value
  incLin++;//increment the cursor

  if(incLin>9){//if we have 10 new values, we count them
    incLin = 0;//since we ended the array we reset the cursor
    float max = 0; //we define a min and max to ba able to remove extreem points
    float min = 360;
    for(int i=0; i <= 9; i ++){
      avg += lin[i];//we add avery values 
      if(lin[i]>max)max = lin[i];//define the min and max value
      if(lin[i]<min)min = lin[i];
    }
    avg = (avg-(min + max))/8;//we remove the min and max and make the average
    
    if(edge){//remove angle shift
      avg = ((int)avg+180)%360 + decimal(avg);
      edge = false;
    }
    return avg;
  }
  return -1;
}
