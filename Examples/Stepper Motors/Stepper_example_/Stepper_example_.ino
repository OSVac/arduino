/*Example sketch to control a 28BYJ-48 stepper motor with ULN2003 driver board, and Arduino UNO. More info: https://www.makerguides.com */
//DON'T WORK (need to .be fixed)
// Include the Arduino Stepper Library:
#include <Stepper.h>

// Number of steps per rotation:
const int stepsPerRevolution = 1024;

//Wiring:
// Pin 12 (D6) to IN1 on the ULN2003 driver
// Pin 13 (D7) to IN2 on the ULN2003 driver
// Pin 14 (D5) to IN3 on the ULN2003 driver
// Pin 4 (D2) to IN4 on the ULN2003 driver

// Create stepper object called 'myStepper', note the pin order:
Stepper myStepper = Stepper(stepsPerRevolution, 14,12, 13,  4);

void setup()
{
  // Set the speed to 5 rpm:
  myStepper.setSpeed(5);
  // Initialize the serial port:
  Serial.begin(9600);
}

void loop() 
{
  // Step one revolution in one direction:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution);
  delay(500);

  // Step one revolution in the other direction:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution);
  delay(500);
}
